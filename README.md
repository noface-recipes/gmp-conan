# gmp-conan

[Conan.io](https://www.conan.io/) package for [gmp library][gmp_home].

This package was created originally by Don Goodman-Wilson (https://github.com/DEGoodmanWilson/conan-gmp).

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
[gmp site][gmp_home] for instruction in how to use the library.


[gmp_home]: https://gmplib.org/

## About gmp

> GMP is a free library for arbitrary precision arithmetic,
> operating on signed integers, rational numbers, and floating-point numbers.

> The main target applications for GMP are cryptography applications and research,
> Internet security applications, algebra systems, computational algebra research, etc.



## License

> Since version 6, GMP is distributed under the dual licenses, GNU LGPL v3 and GNU GPL v2.
