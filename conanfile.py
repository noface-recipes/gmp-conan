from conans import ConanFile
from conans import tools
from conans.tools import download, unzip, check_md5, os_info, pythonpath, environment_append

import os
from os import path

class GmpConan(ConanFile):
    name        = "gmp"
    version     = "6.1.2"
    description = "GMP is a free library for arbitrary precision arithmetic"
    url         = "https://gitlab.com/noface-recipes/gmp-conan.git"
    gmp_url     = "https://gmplib.org/"
    license     = "LGPLv3 or GPLv2"
    settings    =  "os", "compiler", "arch"
    options = {
        "shared": [True, False],
        "use_pic": [True, False],
        "disable_assembly": [True, False],
        "enable_fat": [True, False],
        "enable_cxx": [True, False],
        "disable-fft": [True, False],
        "enable-assert": [True, False]
    }
    default_options = (
        "shared=False",
        "use_pic=False",
        "disable_assembly=False",
        "enable_fat=False",
        "enable_cxx=True",
        "disable-fft=False",
        "enable-assert=False"
    )

    build_requires = (
        "AutotoolsHelper/0.1.0@noface/stable"
    )

    ZIP_FOLDER_NAME = "gmp-%s" % version
    SHA256 = "5275bb04f4863a13516b2f39392ac5e272f5e1bb8057b18aec1c9b79d73d8fb2"

    def source(self):
        zip_name = "gmp-%s.tar.bz2" % self.version
        download("http://gmplib.org/download/gmp/%s" % zip_name, zip_name)
        tools.check_sha256(zip_name, self.SHA256)
        unzip(zip_name)
        os.unlink(zip_name)

    def build(self):
        self.prepare_build()
        self.configure_and_make()

    def package(self):
        SRC = self.ZIP_FOLDER_NAME

        self.copy("*.h", "include", src=SRC, keep_path=True)
        if self.options.shared:
            self.copy(pattern="*.so*", dst="lib", src=SRC, keep_path=False)
            self.copy(pattern="*.dll*", dst="bin", src=SRC, keep_path=False)
        else:
            self.copy(pattern="*.a", dst="lib", src=SRC, keep_path=False)

        self.copy(pattern="*.lib", dst="lib", src=SRC, keep_path=False)
        
    def package_info(self):
        self.cpp_info.libs = ['gmp']


    ##################################################################################################

    def prepare_build(self):
        if getattr(self, "package_folder", None) is None:
            self.package_folder = path.abspath(path.join(".", "install"))
            self._try_make_dir(self.package_folder)

    def configure_and_make(self):
        extra_env = {}

        if self.settings.arch == "x86" and self.settings.os != "Android":
            extra_env["ABI"]="32"

        with tools.chdir(self.ZIP_FOLDER_NAME), pythonpath(self), environment_append(extra_env):
            from autotools_helper import Autotools

            autot = Autotools(self,
               shared      = self.options.shared)

            self.autotools_build(autot)

    def autotools_build(self, autot):
        self.add_options(autot)

        if self.options.use_pic or os_info.is_macos:
            autot.fpic = True

        self.output.info("options = " + str(autot.options))

        autot.configure()
        autot.build()
        autot.install()

    def add_options(self, autot):
        for option_name in self.options.values.fields:
            if not getattr(self.options, option_name) or option_name == "shared":
                continue

            self.output.info("Activate option: %s" % option_name)

            opt = option_name.replace("_", "-").split("-")

            if opt[0] == "enable":
                autot.enable(opt[1])
            elif opt[0] == "disable":
                autot.enable(opt[1])

    def _try_make_dir(self, folder):
        try:
            os.mkdir(folder)
        except OSError:
            #dir already exist
            pass
